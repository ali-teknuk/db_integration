require "test_helper"

describe BlackbaudHooksController do
  it "must get index" do
    get blackbaud_hooks_index_url
    must_respond_with :success
  end

  it "must get show" do
    get blackbaud_hooks_show_url
    must_respond_with :success
  end

  it "must get new" do
    get blackbaud_hooks_new_url
    must_respond_with :success
  end

  it "must get edit" do
    get blackbaud_hooks_edit_url
    must_respond_with :success
  end

end
