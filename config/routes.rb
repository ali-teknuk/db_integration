Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "blackbaud_settings#show"

  resource :blackbaud_settings, only: %i[show create destroy] do
      get :callback, on: :member
  end

  resources :blackbaud_hooks, only: %i(index new create)
end
