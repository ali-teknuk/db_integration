module ApplicationHelper

  def switcher(model, attribute, disabled: false, data: nil)
    value = model.send attribute

    button_tag '', name: "#{model.model_name.param_key}[#{attribute}]",
        value:  (value ? :off : :on), disabled: disabled, data: data,
        class: [(value ? :on : :off), :switcher].join(' ')
  end
end
