class Org < ApplicationRecord
  has_one :blackbaud
  has_many :rest_hooks
  has_many :integrations
  has_many :integration_hooks, through: :integrations

  def blackbaud_enabled?
    blackbaud&.active?
  end

  def email_confirmed?
    true
  end
end
