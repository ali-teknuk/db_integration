class Integration < ApplicationRecord
  belongs_to :org
  has_many :integration_hooks
end
