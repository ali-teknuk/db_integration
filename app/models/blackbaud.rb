class Blackbaud < Integration
  belongs_to :org

  def auth_url(redirect_url)
    oauth_client.auth_code.authorize_url redirect_uri: redirect_url
  end
    
  def exchange_token(code, redirect_url)
    update_attrs oauth_client.auth_code.get_token code, redirect_uri: redirect_url
  end

  private
    
  def update_attrs(oauth_token)
    update access_token:  oauth_token.token,
           refresh_token: oauth_token.refresh_token
  end

 def oauth_client
  OAuth2::Client.new ENV['AUTH_CLIENT_ID'], ENV['AUTH_CLIENT_SECRET'],
        site: 'https://oauth2.sky.blackbaud.com',
        authorize_url: 'https://oauth2.sky.blackbaud.com/authorization',
        token_url: 'https://oauth2.sky.blackbaud.com/token'
  end
end
