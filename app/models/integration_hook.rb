class IntegrationHook < RestHook
  belongs_to :org
  belongs_to :integration

  validates :event_name, uniqueness: { scope: [:org, :target_url] }

  BB_DONOR_MAPPINGS = {
    defaults: {
      type: 'Individual',
      first: :first_name,
      last: :last_name,
      # email: :email,
      # address: :address,
      # phone: :phone,
      # birthdate: :birthdate,
      # lookup_id: :id
      date_added: :created_at,
      gender: :gender,
      religion: :religion
    },
    hook_url: 'https://api.sky.blackbaud.com/constituent/v1/constituents',
    range: {
      type: %w[Individual Organization],
      first: %i[first_name last_name name nickname],
      last: %i[first_name last_name name nickname],
      gender: %w[Male Female Unknown],
      gives_anonymously: [true, false],
      inactive: [true, false],
    },
    required_params: %i[type last],
    non_editable_params: %i[lookup_id date_added gender email address phone birthdate religion],
    editable_params: %i[type first middle last former_name preferred_name
      gives_anonymously inactive],
    # all_params: %i[type last email first address birthdate deceased former_name gender gives_anonymously
    #     inactive lookup_id marital middle name online_presence phone preferred_name suffix suffix_2
    #     title title_2 primary_addressee primary_salutation birthplace ethnicity income religion
    #     industry matches_gifts matching_gift_per_gift_min matching_gift_per_gift_max matching_gift_total_min
    #     matching_gift_total_max matching_gift_factor matching_gift_notes num_employees is_memorial is_solicitor
    #     no_valid_address receipt_type target date_added requests_no_email import_id],
    # available_keys: %i[id first_name last_name email org_id donations_count created_at updated_at
    #     phone address city state zip_code country employer occupation comment last_donation_at
    #     yearly_receipt_sent_at last_zap_sent_at name support_ticket_url has_anonymous_donation
    #     deleted_at merged_into_donor_id merged_donations merged_plans user_id first_donation_id
    #     country_code salutation nickname gender religion birthdate education_level blocked_at]
  }
end
