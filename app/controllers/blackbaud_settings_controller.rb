class BlackbaudSettingsController < ApplicationController
  skip_before_action :verify_authenticity_token
  
  def create
    redirect_to Blackbaud.new.auth_url(callback_blackbaud_settings_url), allow_other_host: true
  end
  
  def callback
    bb = current_org_id.blackbaud || current_org_id.build_blackbaud
    if params[:code].present?
      bb.exchange_token params[:code], callback_blackbaud_settings_url
      current_org_id.blackbaud.update active: true
    end
    redirect_to [:blackbaud_settings]
  end

  def show; end

  def destroy
    current_org_id.blackbaud.update active: false
    redirect_to [:blackbaud_settings]
  end
end
