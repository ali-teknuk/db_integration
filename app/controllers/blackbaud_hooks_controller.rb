class BlackbaudHooksController < ApplicationController
  def index
    @blackbaud_hooks = current_org_id.integration_hooks.where(active: true)
  end

  def new
    @blackbaud_hook = current_org_id.integration_hooks.build
  end

  def create
    @integration = current_org_id.integrations.where(active: true, type: 'Blackbaud').first
    @blackbaud_hook = @integration.integration_hooks.build integration_params
    @blackbaud_hook.target_url = IntegrationHook::BB_DONOR_MAPPINGS[:hook_url]
    @blackbaud_hook.org = current_org_id

    if @blackbaud_hook.save
      flash[:notice] = t '.notice'
      redirect_to blackbaud_hooks_path
    else
      flash[:alert] = error_messages_for @blackbaud_hook
      render :new
    end
  end

  private

  def integration_params
    params.require(:integration_hook).permit(:event_name, mapping_fields: {})
  end
end
