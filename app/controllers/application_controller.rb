class ApplicationController < ActionController::Base
  helper_method :current_org_id
  protect_from_forgery

  def current_org_id
    Org.find_or_create_by lookup_id: cookies[:last_session_org_id]
  end

  def error_messages_for(model)
    model.errors.full_messages.join ', '
  end
end
