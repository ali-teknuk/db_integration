# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_01_27_134156) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "integrations", force: :cascade do |t|
    t.bigint "org_id"
    t.string "type"
    t.string "access_token"
    t.string "refresh_token"
    t.boolean "active", default: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["org_id"], name: "index_integrations_on_org_id"
  end

  create_table "orgs", force: :cascade do |t|
    t.integer "lookup_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "rest_hooks", force: :cascade do |t|
    t.bigint "org_id"
    t.boolean "active", default: true
    t.string "event_name"
    t.string "target_url"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.jsonb "mapping_fields"
    t.bigint "integration_id"
    t.index ["active"], name: "index_rest_hooks_on_active"
    t.index ["event_name"], name: "index_rest_hooks_on_event_name"
    t.index ["integration_id"], name: "index_rest_hooks_on_integration_id"
    t.index ["org_id", "event_name", "target_url"], name: "index_rest_hooks_on_org_id_and_event_name_and_target_url", unique: true
    t.index ["org_id"], name: "index_rest_hooks_on_org_id"
  end

  add_foreign_key "integrations", "orgs"
  add_foreign_key "rest_hooks", "orgs"
end
