class CreateIntegrations < ActiveRecord::Migration[7.0]
  def change
    create_table :integrations do |t|
      t.belongs_to :org, foreign_key: true
      t.string :type
      t.string :access_token
      t.string :refresh_token
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
