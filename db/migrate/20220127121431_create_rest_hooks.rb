class CreateRestHooks < ActiveRecord::Migration[7.0]
  def change
    create_table :rest_hooks do |t|
      t.references :org, index: true, foreign_key: true
      t.boolean :active, index: true, default: true
      t.string :event_name, index: true
      t.string :target_url

      t.timestamps null: false
    end

    add_index :rest_hooks, [:org_id, :event_name, :target_url], unique: true
  end
end
