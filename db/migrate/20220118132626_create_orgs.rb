class CreateOrgs < ActiveRecord::Migration[7.0]
  def change
    create_table :orgs do |t|
      t.string :email
      t.integer :lookup_id

      t.timestamps
    end
  end
end
