class AddIntegrationIdToRestHooks < ActiveRecord::Migration[7.0]
  disable_ddl_transaction!

  def change
    add_reference :rest_hooks, :integration, index: {algorithm: :concurrently}
  end
end
