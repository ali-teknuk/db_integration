class AddMappingFieldsToRestHooks < ActiveRecord::Migration[7.0]
  def change
    add_column :rest_hooks, :mapping_fields, :jsonb
  end
end
